import React, { useState} from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';
import { Cards } from '../Cards';
import { Title } from '../Title';


const PageContainer = styled.div`
background-color: #F7F7F7;

${tw`
  flex
  flex-col
  w-full
  h-full
  items-center
  overflow-x-hidden
`}
input {
  border: none;
  height: 40px;
  width: 40%;
  margin-top: 50px;
  padding: 10px;
}
`;

export function HomePage({userPosts}) {
  const [searchTerm, setSearchTerm] = useState('');
    //Git new branch
    // sprint-22
    //sprint-23
    //fixfix
    //new commit
    // new new
    return <PageContainer>
      <Title/>
      <input 
      type="search" 
      placeholder="Search by title" 
      onChange={(event) => { setSearchTerm(event.target.value)}}
      />
      <Cards 
      userPosts={userPosts} 
      term={searchTerm}
      />
    </PageContainer>
};
import React from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

const CardItem = styled.div`
  -webkit-box-shadow: 0px 10px 13px -7px #000000, -1px 2px 15px 5px rgba(0,0,0,0); 
  box-shadow: 0px 10px 13px -7px #000000, -1px 2px 15px 5px rgba(0,0,0,0);
  ${tw`
    flex
    flex-col
    m-4
    p-6
  `}

  h2 {
    font-size:18px;
    font-weight: 600;
    margin: 0 0 15px 0;
  }

  span {
    font-size: 14px;
    margin-bottom: 15px;
  }
`;

const NickName = styled.div`
  ${tw`
    flex
    flex-row
    justify-between
    w-full
    mb-2
  `}
`;

export function Card({post}) {
  return <CardItem>
    <h2>{post.title}</h2>
    <span>{post.body}</span>
    <NickName>
      <p>{post.username}</p>
      <p>{post.name}</p>
    </NickName>  
    </CardItem> 
};
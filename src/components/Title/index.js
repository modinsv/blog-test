import React from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

const TitleWrapper = styled.div`
 ${tw`
  flex
  items-center
  justify-center
  text-4xl
  md:text-6xl
  font-bold
 `}
`;

export function Title() {
  return <TitleWrapper>
      <p>Posts</p>
    </TitleWrapper>
}
import React from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

import { Card } from '../Card';

const CardsWrapper = styled.div`
  ${tw`
   w-full
   m-2
   p-4
   grid
   grid-cols-4
  `}
`;

export function Cards({userPosts, term}) {
  return <CardsWrapper>
    {userPosts.filter(post => {
      if(term === "") {
          return <Card post={post}/>
      } else if(post.title.toLowerCase().includes(term.toLowerCase())) {
          return <Card post={post}/>
      }
      return null
    }).map((post, index) => (
      <Card key={index} post={post}/>      
     ))}
    </CardsWrapper>
};
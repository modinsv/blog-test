import React, {useState, useEffect} from 'react';
import './App.css';
import styled from 'styled-components';
import tw from 'twin.macro';
import axios from 'axios';

import { HomePage } from './components/HomePage';

const AppContainer = styled.div`
 ${tw`
   w-full
   h-full
   flex
   flex-col
 `}
`;

function App() {
  const [users, setUsers] = useState([]);
  const [posts, setPosts] = useState([]);
 
  useEffect(() => {
    const fetchData = async () => {
      const { data } = await axios(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(data);
    };
    const fetchPosts = async () => {
      const { data } = await axios(
        "https://jsonplaceholder.typicode.com/posts"
      );
      setPosts(data);
    };
    fetchData();
    fetchPosts();
  }, [setUsers, setPosts]);  
  
  let userPosts = posts.map((obj) => {
    let data = users.find((item) => item.id === obj.userId);
    return { ...obj, ...data };
  }); 
  
  return <AppContainer>
    <HomePage userPosts={userPosts}/>
  </AppContainer>
}

export default App;
